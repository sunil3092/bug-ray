﻿using System.ComponentModel.DataAnnotations;

namespace BugRay.Models.DataModels
{
    public class BugModel
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte BugStatus { get; set; }
        public string Comments { get; set; }

    }
}
