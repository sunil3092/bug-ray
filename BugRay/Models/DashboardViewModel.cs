﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BugRay.Models
{
    public class DashboardViewModel
    {
        public List<BugViewModel> Issues { get; set; }
    }
}
