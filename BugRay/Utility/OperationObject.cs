﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BugRay.Utility
{
    public class OperationObject
    {
        public bool OperationValue { get; set; }
        public string Message { get; set; }
    }
}
