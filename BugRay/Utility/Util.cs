﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BugRay.Utility
{
    /// <summary>
    /// Utility class contains methods to extract data from an http request and/or provide a usable data.
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// Parse a string to a DateTime object
        /// </summary>
        /// <param name="text">
        /// Contains DateTimeData as a string
        /// </param>
        /// <returns>
        /// Date time data as an DateTime Object.
        /// </returns>
        public static DateTime ParseDateTime(string text)
        {
            //Date time object
            DateTime dateTime = DateTime.MinValue;

            //Parsing the date time
            dateTime = DateTime.TryParse(text.Trim(), new System.Globalization.CultureInfo("en-GB"), System.Globalization.DateTimeStyles.AdjustToUniversal, out dateTime) ? dateTime : DateTime.MinValue;

            //Returning the dateTime object.
            return dateTime;
        }

        /// <summary>
        /// Utility method to extract string value form an http request.
        /// </summary>
        /// <param name="request">
        /// request object contains the data of the form submitted.
        /// </param>
        /// <param name="fieldName">
        /// fieldname represents the key of the field we need to extract formt he form object in string.
        /// </param>
        /// <returns>
        /// String value of the data.
        /// </returns>
        public static string GetFormFieldValue(HttpRequest request, string fieldName)
        {
            string value = string.Empty;
            if ("" != request.Form[fieldName])
            {
                value = request.Form[fieldName];
            }
            return value;
        }

        public static Guid GetFormFieldGuidValue(HttpRequest request, string fieldName)
        {
            string value = string.Empty;
            Guid guidValue = Guid.Empty;
            if ("" != request.Form[fieldName])
                value = request.Form[fieldName];
            if (!string.IsNullOrEmpty(value))
            {
                Guid.TryParse(value, out guidValue);
            }
            return guidValue;
        }

        public static DateTime GetFormFieldDateValue(HttpRequest request, string fieldName)
        {
            string value = string.Empty;
            if ("" != request.Form[fieldName])
                value = request.Form[fieldName];
            DateTime returnValue = DateTime.MinValue;
            if (!DateTime.TryParse(value, new System.Globalization.CultureInfo("en-GB"), System.Globalization.DateTimeStyles.AdjustToUniversal, out returnValue))
                returnValue = DateTime.MinValue;
            return returnValue;
        }

        public static DateTime GetDateValueFromString(string DateTimeString)
        {
            DateTime returnValue = DateTime.MinValue;
            if (!DateTime.TryParse(DateTimeString, new System.Globalization.CultureInfo("en-GB"), System.Globalization.DateTimeStyles.AdjustToUniversal, out returnValue))
                returnValue = DateTime.MinValue;
            return returnValue;
        }

        public static int GetFormFieldIntValue(HttpRequest request, string fieldName)
        {
            string value = string.Empty;
            int returnValue = 0;
            if ("" != request.Form[fieldName])
                value = request.Form[fieldName];
            if (int.TryParse(value, out returnValue))
                returnValue = Int32.Parse(value);
            return returnValue;
        }

        public static double GetFormFieldDoubleValue(HttpRequest request, string fieldName)
        {
            string value = string.Empty;
            double returnValue = 0.00;
            if ("" != request.Form[fieldName])
                value = request.Form[fieldName];
            if (double.TryParse(value, out returnValue))
                returnValue = double.Parse(value);
            return returnValue;
        }

        public static decimal GetFormFieldDecimalValue(HttpRequest request, string fieldName)
        {
            string value = string.Empty;
            decimal returnValue = decimal.Zero;
            if ("" != request.Form[fieldName])
                value = request.Form[fieldName];
            if (decimal.TryParse(value, out returnValue))
                returnValue = decimal.Parse(value);
            return returnValue;
        }

        public static bool GetFormFieldBooleanValue(HttpRequest request, string fieldName)
        {
            string value = string.Empty;
            if ("" != request.Form[fieldName])
                value = request.Form[fieldName];
            bool returnValue = false;
            if (!bool.TryParse(value, out returnValue))
                returnValue = false;
            return returnValue;
        }

        public static bool GetFormCheckboxValue(HttpRequest request, string fieldName)
        {
            string value = string.Empty;
            bool returnValue = false;
            if ("" != request.Form[fieldName])
            {
                value = request.Form[fieldName];
                if (!string.IsNullOrEmpty(value))
                    returnValue = true;
            }
            return returnValue;
        }
    }
}
