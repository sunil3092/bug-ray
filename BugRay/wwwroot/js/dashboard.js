﻿
$(document).ready(function () {
    populateIssues();
})


function populateIssues() {
    $.post("/Dashboard/GetAllIssues/", function (data) {
        $("#issueDiv").empty();
        $("#issueDiv").html(data);
    })
}

function saveForm() {
    var form = $("#bugForm");

    $.post("/Dashboard/SaveIssue", form.serialize(), function (data) {
        if (data.operationValue === true) {
            bootbox.alert(data.message);
            populateIssues();
        }
        else {
            bootbox.alert(data.message);
        }
    })
}

function deleteIssue(element) {
    var id = $(element).attr("id");
    var index = id.split("_")[1];
    $.post("/Dashboard/DeleteIssue", { id: index }, function (data) {
        if (data.operationValue === true) {
            bootbox.alert(data.message);
            $("#cardId_" + index).remove();
        }
        else {
            bootbox.alert(data.message);
        }
    })
}