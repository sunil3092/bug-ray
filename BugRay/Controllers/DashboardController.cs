﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BugRay.Data;
using BugRay.Models;
using Microsoft.AspNetCore.Mvc;
using BugRay.Models.DataModels;
using System.Text;
using BugRay.Utility;

namespace BugRay.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IMapper _mapper;

        /// <summary>
        /// Application DB context object to acess the DB.
        /// </summary>
        ApplicationDbContext _context;

        /// <summary>
        /// Ctor to provide the inital instansiation.
        /// </summary>
        /// <param name="db"></param>
        public DashboardController(ApplicationDbContext db, IMapper mapper)
        {
            _context = db;
            _mapper = mapper;
        }

        /// <summary>
        /// As DB contect is an imprtant parameter so to prevent misuse dispose it as soon as execution is complete.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }


        /// <summary>
        /// Action method for Dashboard page.
        /// </summary>
        /// <returns>
        /// View
        /// </returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Action method to save the data of the Bug added.
        /// </summary>
        /// <param name="bug">
        /// Obejct containg the inserted bug data
        /// </param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult SaveIssue(BugViewModel bugViewModel)
        {
            StringBuilder stringMessage = new StringBuilder();
            OperationObject operation = new OperationObject();

            var bug = _mapper.Map<BugViewModel, BugModel>(bugViewModel);
            _context.Bugs.Add(bug);

            try
            {
                _context.SaveChanges();
                stringMessage.Append("Data Saved");
                operation.OperationValue = true;
            }
            catch (Exception e)
            {
                stringMessage.Append("Data not Saved due to " + e.Message);
                operation.OperationValue = false;
            }

            operation.Message = stringMessage.ToString();

            return Json(operation);
        }

        public List<BugModel> GetAllIssuesData()
        {
            return _context.Bugs.ToList(); ;
        }

        public IActionResult GetAllIssues()
        {
            StringBuilder stringMessage = new StringBuilder();
            DashboardViewModel model = new DashboardViewModel();
            model.Issues = _mapper.Map<List<BugViewModel>>(GetAllIssuesData());

            return PartialView("_AllIssuesForDashboard", model);
        }

        public IActionResult DeleteIssue(int id)
        {
            StringBuilder stringMessage = new StringBuilder();
            OperationObject operation = new OperationObject();
            try
            {
                BugModel bug = new BugModel();
                bug.Id = id;
                _context.Bugs.Remove(bug);
                _context.SaveChanges();
                operation.OperationValue = true;
                stringMessage.Append("Issue is Deleted");
            }
            catch (Exception e)
            {
                operation.OperationValue = false;
                stringMessage.Append(e);
            }

            operation.Message = stringMessage.ToString();

            return Json(operation);
        }

    }
}