﻿using AutoMapper;
using BugRay.Models;
using BugRay.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BugRay.Data
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<BugModel, BugViewModel>();
            CreateMap<BugViewModel, BugModel>();
        }
    }
}
